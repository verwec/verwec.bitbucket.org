$(document).ready(function(){

  $('button.book').on('click', function(){
    var price = $(this).closest('.tour').data('price');
    var newPrice = $(this).closest('.tour').find('.price').text()

    if(newPrice == ''){
      newPrice = price;
    }

    newPrice = parseInt(newPrice) + 1;

    var content = "<div>" + newPrice + "</div>";
    $(this).closest('.tour').find('.price').html(content)
  });
});
