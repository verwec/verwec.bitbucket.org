function showDetails(){
 $(this).closest('.tour').find('.details').show()
}

$(document).ready(function(){
  $('button').on('click', showDetails);
  // $('button').on('mouseenter', showDetails);


  // calculate price
  $('.quantity').on('keyup', function(){
    var newQuantity = $(this).val()
    var pricePerUnit =$('.price').data('price')

    $('.price').text(newQuantity*pricePerUnit)
  });
});
